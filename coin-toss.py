# Cointoss

# Importing random module
import random

# Setting varitables
flips = int(100)
h = int(0)
t = int(1)
heads = int(0)
tails = int(0)

print("Digital coin fliping in progress.\n")

# Loop body
while flips != (0):

    landing = random.randint(0,1)

    if landing == h:
        heads += 1
        
        #print("\nHeads")

    elif landing == t:
        tails += 1
       
        #print("\n \t Tails")

    flips = (flips - 1)

# Printing values
print("Heads\t Tails")
print(heads,"\t",tails)

# Ending program
input("\n\nPress enter key to exit")
